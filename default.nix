{ lib, writeShellScriptBin, makeWrapper

, libnotify
, ntfy-sh
, python310
}:

let
  py = python310.withPackages (pp: with pp; [ emoji ]);
in

writeShellScriptBin "ntfy-bridge" ''
  if [[ "$#" -ne 1 ]]; then
    echo >&2 "Usage: $(basename "$0") [host/]<topic_to_subscribe>"
    echo >&2 "  If <host/> is ommitted, a default host is determined by reading"
    echo >&2 "  the \`default-host\` field in the ~/.config/ntfy/client.yml"
    echo >&2
    echo >&2 "  E.g. $(basename "$0") ntfy.lan/some_topic"
    exit 1
  fi
  export PATH=${libnotify}/bin''${PATH:+:$PATH}
  ${ntfy-sh}/bin/ntfy subscribe "$1" \
    | ${py}/bin/python3 ${./ntfy-bridge.py}
''
