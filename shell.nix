{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    ntfy-sh
    (python310.withPackages (pp: with pp; [
      emoji
    ]))
  ];
  shellHook = ''
    export PYTHONUNBUFFERED=1
    export PYTHONDONTWRITEBYTECODE=1
    export PYTHONBREAKPOINT=ipdb.set_trace
    [[ "$-" == *i* ]] && exec "$SHELL"
  '';
}
