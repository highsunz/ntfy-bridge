{
  description = "Simple script that routes ntfy notifications to Linux desktop using libnotify";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [
        self.overlays.default
      ];
    };
  in {
    packages = rec {
      inherit (pkgs) ntfy-bridge;
      default = ntfy-bridge;
    };
    devShells = rec {
      default = ntfy-bridge;
      ntfy-bridge = pkgs.callPackage ./shell.nix {};
    };
  }) // {
    overlays.default = final: prev: {
      ntfy-bridge = final.callPackage ./. {};
    };
    hydraJobs = self.packages;
    hmModules = rec {
      default = ntfy-bridge;
      ntfy-bridge = (import ./modules.nix).homeManager;
    };
  };
}
