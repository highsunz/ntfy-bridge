{
  homeManager = { config, lib, pkgs, ... }: let
    cfg = config.services.ntfy-bridge;
  in with lib; {
    options.services.ntfy-bridge = {
      enable = mkEnableOption ''
        Whether to listen for notifications from ntfy and send them to desktop
      '';
      defaultHost = mkOption { type = with types; nullOr str; default = null; };
      topics = mkOption { type = with types; listOf str; default = []; };
    };

    config = mkIf cfg.enable {
      systemd.user.services = let
        makeService = input: let
          topic = head (reverseList (splitString "/" input));
          hostSlashTopic = with builtins; if (length (splitString "/" topic)) > 1
            then topic
            else "${cfg.defaultHost}/${topic}";
        in {
          name = "ntfy-bridge-${builtins.replaceStrings [ "/" ":" ] [ "." "." ] hostSlashTopic}";
          value = {
            Service = {
              ExecStart = "${pkgs.ntfy-bridge}/bin/ntfy-bridge ${hostSlashTopic}";
              Restart = "always";
              RestartSec = 5;
            };
            Install.WantedBy = [ "graphical-session.target" ];
          };
        };
        makeServices = topics: builtins.listToAttrs (map makeService topics);
      in makeServices cfg.topics;
    };
  };
}
