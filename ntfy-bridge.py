#!/usr/bin/env python3

import json
import subprocess
import warnings

import emoji


def handle_message(payload):
    title = "/" + payload["topic"] + ":"
    message = payload["message"]
    if "title" in payload:
        title += " " + payload["title"]
    if "tags" in payload:
        emoji_tags = "{}".format(" ".join(map(lambda tag: ":{}:".format(tag), payload["tags"])))
        title = emoji.emojize(emoji_tags, language="alias") + " " + title
    if "priority" in payload:
        urgency = [
            "low",
            "normal",
            "normal",
            "critical",
            "critical",
        ][max(0, min(5, payload["priority"] - 1))]
    else:
        urgency = "normal"
    return subprocess.run([
        "notify-send",  # needs notify-send in PATH
        "--urgency={}".format(urgency),
        "--icon={}".format("guake"),
        title,
        message,
    ])

def main():
    print("awaiting inputs from stdin ...")
    while True:
        try:
            raw = input()
            blob = json.loads(raw)
        except json.JSONDecodeError as e:
            warnings.warn(
                "Ignoring decode error on {}, the original error was:\n{}".format(str(e)))
            continue
        except KeyboardInterrupt:
            print("exiting gracefully")
            return 0

        if blob["event"] == "message":
            print(handle_message(blob))
        else:
            warnings.warn(
                "unimplemented event: {}".format(blob["event"]))
    return 0


if __name__ == "__main__":
    exit(main())
